package se.sensera.edu.grouprest.grouprest;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.ExternalDocumentation;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Configuration
public class OpenDocConfig {

    @Bean
    public OpenAPI springShopOpenAPI() {
        final String securitySchemeName = "bearerAuth";

        return new OpenAPI()
                .info(new Info().title("Sensera Education Group Rest API")
                        .description("### Simple group API for education and teaching. \n" +
                                "To access this resource you need to acquire a token from [Sensera auth](https://iam.sensera.se) \n" +
                                "\n" +
                                "    Realm: test\n" +
                                "    Client Id: group-api\n" +
                                "    Username: user\n" +
                                "    Password: djnJnPf7VCQvp3Fc\n" +
                                "\n" +
                                "[About the keycloak api](https://www.keycloak.org/docs-api/9.0/rest-api/index.html)\n" +
                                "\n" +
                                "[Source can be found at](https://gitlab.com/sensera-education-public/Groups-Rest)")
                        .version("v1.0.0")
                        .license(new License().name("GNU General Public License v3.0").url("https://www.gnu.org/licenses/gpl-3.0.html")))
                .addSecurityItem(new SecurityRequirement().addList(securitySchemeName))
                .components(
                        new Components()
                                .addSecuritySchemes(securitySchemeName,
                                        new SecurityScheme()
                                                .name(securitySchemeName)
                                                .type(SecurityScheme.Type.HTTP)
                                                .scheme("bearer")
                                                .bearerFormat("JWT")
                                )
                );
    }
}
